package com.example.demo.service;

import com.example.demo.model.Costumer;
import com.example.demo.repository.CostumerRepository;
import com.example.demo.repository.HibernateCostumerRepositoryImpl;

import java.util.List;

public class CostumerServiceImpl implements CostumeService {
    private CostumerRepository costumerRepository = new HibernateCostumerRepositoryImpl();

    @Override
    public List<Costumer> findAll() {
        return costumerRepository.findAll();
    }
}
