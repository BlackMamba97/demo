package com.example.demo.service;

import com.example.demo.model.Costumer;

import java.util.List;

public interface CostumeService {
    List<Costumer> findAll();
}
