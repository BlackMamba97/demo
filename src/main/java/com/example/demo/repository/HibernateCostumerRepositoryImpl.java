package com.example.demo.repository;

import com.example.demo.model.Costumer;

import java.util.ArrayList;
import java.util.List;

public class HibernateCostumerRepositoryImpl implements CostumerRepository {

    @Override
    public List<Costumer> findAll() {
        List<Costumer> costumers = new ArrayList<>();
        Costumer costumer = new Costumer();
        costumer.setFirstname("dio");
        costumer.setLastname("cane");
        costumers.add(costumer);
        return costumers;
    }
}
