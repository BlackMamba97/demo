package com.example.demo.repository;

import com.example.demo.model.Costumer;

import java.util.List;

public interface CostumerRepository { // utilizzato per tenere i metodi di istanza sul db per costumer
    List<Costumer> findAll();
}
