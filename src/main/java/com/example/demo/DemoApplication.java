package com.example.demo;

import com.example.demo.model.Costumer;
import com.example.demo.service.CostumeService;
import com.example.demo.service.CostumerServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        CostumeService service = new CostumerServiceImpl();
        Costumer costumer = service.findAll().get(0);
        System.out.println(costumer.getFirstname());
        System.out.println(costumer.getLastname());
    }

}
